#pragma once
#include <iostream>
#include <fstream>
#include <string>
struct node                                                
{                                                               
      int score_;               // will store information
      std::string name_;
      node *next;             // the reference to the next node
};          

class HighScoreList {
private:
  int number_of_scores_;
  node * head;
public:
  HighScoreList();
  HighScoreList(std::ifstream &input);
  ~HighScoreList();
  int get_number_of_nodes();
  node * get_head();
  void add_score(std::string par_name, int par_score);
  void save_score();
  void sort_list();
  std::string return_highscore_list ();

};