 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "highscorelist.h"
#include <fstream>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <Windows.h>
 
#include "SDL_net.h"
void add_highscore(char* par_buffer, HighScoreList* par_high_score_list);
 
int main(int argc, char **argv)
{
	TCPsocket sd, csd; /* Socket descriptor, Client socket descriptor */
	IPaddress ip, *remoteIP;
	int quit, quit2;
	char buffer[512];
  HighScoreList *high_score_list;
  high_score_list = new HighScoreList();
  std::ifstream input("highscore.txt");
  if(input.is_open()) {
    if(input.peek() != EOF)
    HighScoreList *temp = high_score_list;
     high_score_list = new HighScoreList(input);
    //delete temp;
  }
  
    

 
	if (SDLNet_Init() < 0)
	{
		fprintf(stderr, "SDLNet_Init: %s\n", SDLNet_GetError());
		exit(EXIT_FAILURE);
	}
 
	/* Resolving the host using NULL make network interface to listen */
	if (SDLNet_ResolveHost(&ip, NULL, 2000) < 0)
	{
		fprintf(stderr, "SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		exit(EXIT_FAILURE);
	}
 
	/* Open a connection with the IP provided (listen on the host's port) */
	if (!(sd = SDLNet_TCP_Open(&ip)))
	{
		fprintf(stderr, "SDLNet_TCP_Open: %s\n", SDLNet_GetError());
		exit(EXIT_FAILURE);
	}
 
	/* Wait for a connection, send data and term */
	quit = 0;
	while (!quit)
	{
		/* This check the sd if there is a pending connection.
		* If there is one, accept that, and open a new socket for communicating */
		if ((csd = SDLNet_TCP_Accept(sd)))
		{
			/* Now we can communicate with the client using csd socket
			* sd will remain opened waiting other connections */
 
			/* Get the remote address */
			if ((remoteIP = SDLNet_TCP_GetPeerAddress(csd)))
				/* Print the address, converting in the host format */
				printf("Host connected: %x %d\n", SDLNet_Read32(&remoteIP->host), SDLNet_Read16(&remoteIP->port));
			else
				fprintf(stderr, "SDLNet_TCP_GetPeerAddress: %s\n", SDLNet_GetError());
 
			quit2 = 0;
      
//      int score;
			while (!quit2)
			{                                      
        if(SDLNet_TCP_Recv(csd, buffer, 512) > 0) {
          if(buffer[0] == '#') {
            add_highscore(&buffer[1], high_score_list);
          }
          if(buffer[0] == '�') {
           std::string temp_string = high_score_list->return_highscore_list();
           //std::cout<<temp_string;
            if (SDLNet_TCP_Send(csd, (void *)temp_string.c_str(), strlen(temp_string.c_str())+1) < strlen(temp_string.c_str())+1) {
			        fprintf(stderr, "SDLNet_TCP_Send: %s\n", SDLNet_GetError());
			        exit(EXIT_FAILURE);
		        }
          }
          if(strcmp(buffer,"terminate")) {
            std::cout<<"connection terminated\n";
            quit2 = 1;
          }


        } 
				
			}
 
			/* Close the client socket */
			SDLNet_TCP_Close(csd);
		}
    Sleep(200);
	}
 
	SDLNet_TCP_Close(sd);
	SDLNet_Quit();
 
	return EXIT_SUCCESS;
}

void add_highscore(char* par_buffer, HighScoreList* par_high_score_list) {
  bool traverse = true;
  int traverse_number = 0;
  int traverse_number_score = 0;
  char name_[20];
  char score_[30];
  traverse_number = 0;
  traverse_number_score = 0;
  while(traverse) {
    if(par_buffer[traverse_number] == '$') {
      for(int i = 0; i < traverse_number; i++) {
        name_[i] = par_buffer[i]; 
       }
      traverse = false;
      name_[traverse_number] = '\0';
      }
      traverse_number++;

      }
      traverse = true;
      while(traverse) {
        if(par_buffer[traverse_number] <= 57 && par_buffer[traverse_number] >= 48){
          score_[traverse_number_score] = par_buffer[traverse_number];
        }
        else {
          traverse = false;
          score_[traverse_number_score]='\0';
        }
        if(par_buffer[traverse_number]=='$') {
          traverse = false;
        }
        traverse_number++;
        traverse_number_score++;           
        }
        std::cout<<"navn: " << name_ <<"\tscore: " << score_ <<"\n";
        par_high_score_list->add_score(std::string(name_),atoi(score_));
        par_high_score_list->save_score();
        
}