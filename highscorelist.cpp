#include "highscorelist.h"
#include <algorithm>


HighScoreList::HighScoreList() {
  head = new node();
  head->name_= "";
  head->score_= 0;
  head->next = nullptr;
  number_of_scores_ = 0;
}
HighScoreList::HighScoreList(std::ifstream &input){
   head = new node();
   input >> number_of_scores_;
   node* temp_node;
   temp_node = new node();
   input >> temp_node->name_;
   input >> temp_node->score_;
   head = temp_node;
   temp_node = new node();
   head->next = temp_node;
   for(int i = 1; i < number_of_scores_; i++) {
      input >> temp_node->name_;
      input >> temp_node->score_;
      temp_node->next = new node();
      temp_node = temp_node->next;
   }
}
HighScoreList::~HighScoreList() {
  node* temp1;
  temp1=(node*)malloc(sizeof(node));
  while(head->next != NULL) {
    temp1 = head;
    head = temp1->next;
    delete temp1;
  }
  delete head;
    
}
int HighScoreList::get_number_of_nodes() {
  return number_of_scores_;
}
node * HighScoreList::get_head() {
  return head;
}
//Adds a score, will keep the list sorted aslong as the list already is sorted.
//which we accomplish by having the txt containing 10 already sorted scores.
void HighScoreList::add_score(std::string par_name, int par_score) {
  node *temp_node = new node();
  temp_node->score_ = par_score;
  temp_node->name_ = par_name;
  if(number_of_scores_ == 0) {
    head = temp_node;
    head->next = nullptr;
  }
  else if(head->score_ < temp_node->score_) {
    temp_node->next = head;
    head = temp_node;
  }
  else {
    node * node_before = head;
    while(node_before->score_ > temp_node->score_ && node_before->next != nullptr) {
      node_before = node_before->next;
    }
    temp_node->next = node_before-> next;
    node_before->next = temp_node;
  }
  if(number_of_scores_ > 10) {
    node * currentPtr = head;
    for(int i = 0; i < 9; i++) {
      currentPtr = currentPtr->next;  
    }
    currentPtr->next = nullptr;
  }
   
}
void HighScoreList::save_score() {
  std::ofstream output("highscore.txt");
  node* temp_node;
  temp_node = (node*)malloc(sizeof(node));
  if(output.is_open()) {
    output<<number_of_scores_ << "\n";
    output<<head->name_<<"\t"<<head->score_<< "\n";
    temp_node=head->next;
    for(int i = 1; i<number_of_scores_; i++) {
      output<<temp_node->name_ <<"\t"<< temp_node->score_<<"\n";
      temp_node=temp_node->next;
    }
    output.close();
  }
}
void HighScoreList::sort_list() {
  

}
std::string HighScoreList::return_highscore_list () {
  std::string temp_string;
  node* temp_node;
  int test=0;
  temp_node = (node*)malloc(sizeof(node));
  temp_string.append(std::to_string(number_of_scores_));
  temp_string.append("$");
  temp_string.append(head->name_);
  temp_string.append("$");

  temp_string.append(std::to_string(head->score_));
  temp_node = head->next;
  if(number_of_scores_ == 1)
    temp_string.append("$");
  for(int i = 1; i < number_of_scores_; i++) {
    test = i;
    temp_string.append("$");
    temp_string.append(temp_node->name_);
    temp_string.append("$");
    temp_string.append(std::to_string(temp_node->score_));
    temp_node = temp_node->next;    
  }
  temp_string.append("$");
  temp_string.append("\0");

  return temp_string;
}        
